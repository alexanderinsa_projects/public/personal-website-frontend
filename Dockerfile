FROM node:12.18.2-alpine3.12 as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
ARG PUBLIC_URL
ENV PUBLIC_URL=$PUBLIC_URL

COPY package.json ./
COPY package-lock.json ./

RUN npm ci --silent
RUN npm install react-scripts@4.0.1 -g --silent

COPY . ./
RUN npm run build


FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]