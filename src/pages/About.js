import React from 'react';

import './About.scss'

function About(props) {
  return(
    <div>
      <h1>This is the About Page</h1>
      <p>This is a site where I can experiment with writing web applications</p>
    </div>
  );
}

export default About;
