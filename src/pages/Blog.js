import React from 'react';

import './Blog.scss'

function Blog(props) {
  return(
    <div>
      <h1>This is the Blog Page</h1>
      <p>This is a site where I can experiment with writing web applications</p>
    </div>
  );
}

export default Blog;
