import React from 'react';

import './Home.scss';

// const Header = () => (
//   <Navbar bg="primary" expand="lg">
//   <Navbar.Brand className="text-light" href="#home">amorgenstern.xyz</Navbar.Brand>
//   <Nav className="ml-auto">
//     <Nav.Link className="text-light" href="#about">About</Nav.Link>
//   </Nav>
//   <Navbar.Toggle aria-controls="basic-navbar-nav" />
//   </Navbar>
// )

function Home(props) {
  return(
    <div>
      {/* <Header /> */}
      <h1>A Website to Practice and Showcase Building Software</h1>
      Welcome to my page!
    </div>
  );
}

export default Home;
