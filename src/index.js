import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

import './index.scss';
import * as serviceWorker from './serviceWorker';
import { render } from '@testing-library/react';


ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
