import React, { useState } from 'react';
import { Link } from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const Header = () => {
  return (
    <Navbar bg="primary" expand="lg">
    <Navbar.Brand className="text-light" as={Link} to="/">amorgenstern.xyz</Navbar.Brand>
    <Nav className="ml-auto">
      <Nav.Link as={Link} to="/about" className="text-light">About</Nav.Link>
      <Nav.Link as={Link} to="/blog" className="text-light">Blog</Nav.Link>
      <Nav.Link as={Link} to="/thinking" className="text-light">Thinking</Nav.Link>
    </Nav>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    </Navbar>
  );
};

export default Header