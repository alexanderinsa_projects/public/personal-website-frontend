import React from 'react';
import Header from './components/Header'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './pages/Home'
import About from './pages/About'
import Blog from './pages/Blog'
import Thinking from './pages/Thinking'
import FilterableProductTable from './pages/Thinking'

import './App.scss';

// function App(props) {
//   return(
//     <div>
//       {/* <Header /> */}
//       <h1>A Website to Practice and Showcase Building Software</h1>
//       Welcome to my page!
//     </div>
//   );
// }

function App() {
  return (
    <BrowserRouter>
      <Header />
      <div className="container mt-2" style={{ marginTop: 40 }}>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/blog">
            <Blog />
          </Route>
          <Route path="/thinking">
            <FilterableProductTable />
            {/* <Thinking /> */}
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
